import pandas as pd
import numpy as np

# Load the data from the Excel workbook
sitelist = pd.read_excel("Data/Windfarms_World_20191212.xls", sheet_name="Windfarms")

# Filter the rows to keep only the ones where the "country" column is equal to "Netherlands"
sitelist = sitelist[sitelist["Country"] == "Netherlands"]

sitelist = sitelist[sitelist["Status"].isin(["Production", "Construction", "Approved", "Planned"])]

#Remove columns not needed
sitelist = sitelist.drop(columns=["Continent", "ISO code", "State code", "City", "2nd name", "Altitude/Depth", "Location accuracy", "Decommissioning date", "Link", "Update", "Developer", "Operator", "Owner" ])

#add a site id to the first column
sitelist.insert(0, "id", range(1, len(sitelist) + 1))

# Convert the column "Hub height" to numeric values, replacing non-numeric values with NaN
sitelist["Hub height"] = pd.to_numeric(sitelist["Hub height"], errors='coerce')

# Replace NaN values in the "Hub height" column with the mean of the column
sitelist["Hub height"].fillna(sitelist["Hub height"].mean(), inplace=True)

sitelist["Offshore"] = sitelist["OffshoreShore distance"] != "No"

sitelist["Technology"] = np.where(sitelist["OffshoreShore distance"] == "No", "won", "wof")

sitelist["Turbine"].replace("#ND", np.nan, inplace=True)

sitelist["Manufacturer"].replace("#ND", np.nan, inplace=True)

sitelist["Turbinename"] = sitelist["Manufacturer"].astype(str)+" "+sitelist["Turbine"].astype(str)


# Show the first 5 rows of the data

print(sitelist.head())